package com.example.rent.sda_020_ap_person_listview_data_binding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-05-04.
 */

public class PersonProvider {

    public List<Person> provide() {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Jan", 20));
        persons.add(new Person("Halina", 25));
        persons.add(new Person("Adam", 9));
        return persons;
    }
}